# vpncon

## About

**vpncon** is a little Apple script, which manages [Tunnelblick](https://tunnelblick.net/)'s VPN connections: it brings up a list of all saved VPNs and allows connecting and re-connecting to those.

## Usage

Just run the [compiled script](https://bitbucket.org/dsjkvf/applescript-vpncon/downloads/vpncon.scpt), or the [shell implementation](https://bitbucket.org/dsjkvf/applescript-vpncon/downloads/vpncon.sh) of it.
