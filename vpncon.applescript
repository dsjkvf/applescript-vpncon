-- vim: set ft=applescript:
-- HELPERS

-- connect
to vpnCon(vpn_name)
    tell application "Tunnelblick"
        connect vpn_name
        repeat
            delay 1
            set vpn_state to state of first configuration where name = vpn_name
            if vpn_state is "CONNECTED" then
                display dialog (vpn_name & ": " & vpn_state) as string giving up after 2
                exit repeat
            end if
        end repeat
    end tell
end vpnCon

-- disconnect (may need in the future)
to vpnDis(vpn_name)
    tell application "Tunnelblick"
        disconnect vpn_name
    end tell
end vpnDis

-- if connected
on isCon()
    tell application "Tunnelblick"
        set rez to the state of configurations
        if "CONNECTED" is in rez then
            return true
        end if
        return false
    end tell
end isCon

-- proceed with connecting
on procCon()
    -- get a list of all possible VPN connections
    tell application "Tunnelblick"
        set vpn_configs to get configurations
    end tell
    set vpn_names to {}
    repeat with i from 1 to count of vpn_configs
        tell application "Tunnelblick"
            set end of vpn_names to get name of configuration i
        end tell
    end repeat
    -- define UI
    set vpn_sel to (choose from list vpn_names with title "Select VPN" with prompt "         Choose a connection:")
    -- choose
    if vpn_sel is false then
        return
    else if (vpn_sel is in vpn_names) then
        try
            my vpnCon(vpn_sel)
        end try
    else
        display dialog "Gone wrong!"
    end if
end procCon

-- MAIN

-- check the state
if my isCon() then
    --if connected then warn first
    set if_run to (button returned of (display dialog "There is an active connection. Exit, start another?" buttons {"No", "Yes"} default button "No"))
    if if_run is "No" then
        error number -128
    else
        -- proceed
        tell application "Tunnelblick" to disconnect all
        my procCon()
    end if
else
    -- otherwise proceed
    my procCon()
end if
